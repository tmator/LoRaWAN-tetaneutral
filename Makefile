THEME=flask

FLAGS=-a data-uri -a theme=$(THEME)

SRCS=$(wildcard *.adoc)
OBJS=$(patsubst %.adoc, html/%.html, $(SRCS))

.PHONY: all
all: $(OBJS)

html/%.html: %.adoc
	@mkdir -p images html src
	@echo "ASCIIDOCTOR     $(basename $@).html"
	@asciidoctor -o $(basename $@).html $(FLAGS) $<
	
.PHONY: clean
clean:
	rm -f html/*
