![Build Status](https://framagit.org/BOULLE/LoRaWAN-tetaneutral/badges/master/build.svg)

# Le projet

Le but est de créer une extension du projet et de la philosophie de [tetaneutral](http://www.tetaneutral.net/) à l'internet des objets.

Il y a dans ce projet une portée pédagogique importante avec l'intérêt
de plusieurs enseignants et établissements de la région.

Nous avons décider de scinder l'architecture en deux :

- Une plateforme data qui accueille une base de donnée et une
application web type grafana afin de les visualiser simplement.

- Un serveur de réseau LoRaWAN afin de centraliser les flux de données
qui proviennent des gateways et de les enregistrer sur la plateforme data.

L'idée est d'avoir plusieurs gateways chez des adhérents ou dans des
établissements afin d'avoir une couverture assez large, ce qui est
facilité par le fait que la technologie LoRaWAN est longue portée (~5km
en ville).

Le tout dans la philosophie du **logiciel libre**.

Nous avons une dizaine de passerelles en cours de configuration.

Une fois que l'on aura déployé plusieurs gateways, nous serons en mesure de fournir des
objets type arduino afin de pouvoir pousser facilement des données sur la plateforme de visualisation. Il sera également possible de développer tout type d'usage autour des ces données.

Ce projet est soutenu par : Tétaneutral, Snootlab, l'IUT de Blagnac, LinuxÉdu, Linux-Tarn, leurs sympathisants, des enseignants de collèges, lycées et universités...

Join us !

https://lists.tetaneutral.net/listinfo/iot

https://kiwiirc.com/client/irc.freenode.net?chan=#tetaneutral.net

https://boulle.frama.io/LoRaWAN-tetaneutral/