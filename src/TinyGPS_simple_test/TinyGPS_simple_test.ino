#include <TinyGPS.h>
TinyGPS gps;

void setup()
{
  SerialUSB.begin(115200); // Console is on SerialUSB
  Serial1.begin(9600); // GPS is on Serial1
}

void loop()
{
  bool newData = false;

  while (Serial1.available())
  {
    // Feed TinyGPS with NMEA messages
    char c = Serial1.read();
    //SerialUSB.write(c); // uncomment this line if you want to see the GPS data flowing
    if (gps.encode(c)) newData = true; // Set newData flag if a new GPS data is available
  }

  if (newData)
  {
    // If a new GPS data is available, print latitude, longitude, number of satellites and HDOP
    float flat, flon;
    unsigned long age;
    gps.f_get_position(&flat, &flon, &age);
    SerialUSB.print("LAT=");
    SerialUSB.print(flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat, 6);
    SerialUSB.print(" LON=");
    SerialUSB.print(flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon, 6);
    SerialUSB.print(" SAT=");
    SerialUSB.print(gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
    SerialUSB.print(" PREC=");
    SerialUSB.print(gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
    newData = false;
  }
}
