#ajouter la lib paho : pip install paho
import paho.mqtt.client as mqtt
import json
import base64
import logging

#config
mqttServer="loraserver.tetaneutral.net"
appID="0"
deviceID="0000000000000000"

logging.basicConfig(level=logging.DEBUG)

def on_connect(client, userdata, flags, rc):
    mqttc.subscribe("application/"+appID+"/device/"+deviceID+"/rx", 0)

#callback appele lors de la reception d un message
def on_message(mqttc,obj,msg):
    jsonMsg = json.loads(msg.payload)
    device = jsonMsg["devEUI"]
    gw = jsonMsg["rxInfo"][0]["gatewayID"]
    rssi = jsonMsg["rxInfo"][0]["rssi"]
    data = base64.b64decode(jsonMsg["data"])
    print("dev id" + device + ", gw id" + gw + ", data " + data  + ", rssi "+ str(rssi)   )
 
    #on publie un message pour envoyer des donnees au node (pong en base64)
    msgstr = r'''{ "reference": "abcd1234", "confirmed": true, "fPort": 10, "data": "cG9uZwo=" }'''
    mqttc.publish("application/"+appID+"/device/"+deviceID+"/tx",msgstr)

def on_publish(client, userdata, mid):
    print("Message publie..." + str(mid))


mqttc = mqtt.Client()

#les callback
mqttc.on_message=on_message
mqttc.on_connect=on_connect
mqttc.on_publish=on_publish

logger = logging.getLogger(__name__)

mqttc.enable_logger(logger)

mqttc.connect(mqttServer, 1883, 60)


mqttc.loop_forever()
