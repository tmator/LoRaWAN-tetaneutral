---
title: Foire Aux Questions"
tags: ["loRa", "LoRaWAN", "FAQ"]
date: "2018-11-03"
---

:data-uri:
:icons: font
:toc: left
:toclevels: 3
:imagesdir: ./images
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

= FAQ

LoRa et LoRaWAN, ce n'est pas la même chose?:: LoRa (_Long Range_) est une technologie radio permettant à des équipements de transmettre des informations sans fil. C'est une technique de modulation radio basée sur une modulation à étalement de spectre (CSS). Le brevet de cette technologie appartient désormais à la société Semtech (Californie) suite au rachat en 2012 de la société grenobloise, Cycléo, qui l'avait déposé. LoRaWAN™ est un protocole  _Low Power Wide Area Network (LPWAN)_ basé sur LoRa pour les objets connectés sans fils sur batterie dans un réseau régional, national ou mondial. LoRaWAN permet de la communication bi-directionnelle sécurisée (chffrement AES128) ainsi que de la localisation.

Qu'est ce que MQTT ?:: TODO

Qu'est ce que Node-RED?:: TODO

À quoi sert LoRaWAN ?:: TODO

Pourquoi n'êtes vous pas partis sur SigFOX ?:: TODO

Est-ce que c'est sécurisé ?:: TODO

Combien ça coûte ?:: TODO

Je ne suis pas développeur, comment puis-je m'en servir ?:: TODO

À quoi sert une passerelle ?:: TODO
