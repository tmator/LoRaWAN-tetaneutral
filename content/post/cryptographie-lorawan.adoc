---
title: Cryptographie en LoRAWAN
tags: ["cryptographie", "LoRaWAN", "AES", "sécurité", "NwkSkey", "AppSKey"]
date: "2018-11-05"
---

:data-uri:
:icons: font
:toc: left
:toclevels: 3
:imagesdir: ./images
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

= Sécurité en LoRaWAN

WARNING: Document en cours de rédaction...

== Sécurité ?

Ce que l'on appelle *sécurité* d'un système d'information repose sur quatre piliers :

* *confidentialité* : l'information ne peut être lue par une personne non autorisée. Il doit être impossible (ou très difficile) de lire les données transmises en LoRaWAN
* *authenticité* : l'information est attribuée à son auteur légitime. On doit savoir avec certitude quel noeud a envoyé l'information.
* *intégrité* : l’information ne peut être modifiée par une personne non autorisée. L'information transmise ne peut pas être modifiée ou cela doit se voir immédiatement.
* *non-répudiation* : l'information ne peut faire l'objet d'un déni de la part de son auteur. Dans notre contexte, il faut que les données aient été envoyée et reçues par les bons matériels.

TIP: Plus d'informations sur https://www.ssi.gouv.fr/particulier/bonnes-pratiques/crypto-le-webdoc/cryptologie-art-ou-science-du-secret/ et aussi https://www.cnil.fr/fr/comprendre-les-grands-principes-de-la-cryptologie-et-du-chiffrement

Voici les méthodes utilisées, en général, pour assurer ces quatre fondamentaux :

* confidentialité : chiffrement par cryptographie symétrique (à clé secrète) comme 3DES, AES... ou asymétrique (à clé publique et privée) comme RSA, ECC...
* authenticité : algorithmes de signature numérique basés sur du chiffrement
* intégrité : algorithmes de hachage comme SHA-1, MD5 ou SHA-256. On peut aussi utiliser du chiffrement.
* non-répudiation : certificat numérique là encore basé sur du chiffrement.

== Sécurité en LoRaWAN (OTAA)

[TIP]
.OTAA ?
=====
Au démarrage, le noeud tente de se connecter par lui-même en envoyant une requête dite de "_join_" au serveur LoRaWAN via la passerelle.
Si le serveur l'accepte, deux clés dites de session sont négociées et générées à partir d'un "secret commun". Ce secret commun, _AppKey_ est stocké à la fois sur le noeud (en dur dans le code) et sur la passerelle. Ce processus est appelé OTAA (_Over the air activation_).
=====

Le but du chiffrement ici est d'assurer l'identification de l'objet, l'intégrité du message et sa confidentialité.

Le protocole LoRaWAN v1.1 est basé sur de la cryptographie symétrique (AES128). Il utilise des clés de sessions. Une clé de session est une clé symétrique utilisée tout au long de la communication.

En LoRaWAN, deux clés de sessions sont générées lors de la procédure de _join_ durant laquelle l'objet s'authentifie et se connecte au réseau.

Ces clés sont dérivées des clés principales définies dans l'objet (_"To secure radio transmissions the LoRaWAN protocol relies on symmetric cryptography using session keys derived from the device’s root keys"_).

[TIP]
Le chiffrement asymétrique bien qu'en théorie plus sécurisé est trop lent et trop consommateur d'énergie. Ce n'est pas adapté aux contraintes énergétiques des objets.

Voici l'architecture simplifiée :

image:lorawan-security.png[]

Comme on peut le voir, il y a deux clés de sessions :

* *NetworkSessionKey (NwkSkey)* qui assure l'identification (empêche des attaques du type "homme du milieu" ou une modification des messages à la volée). Elle utilise un clé AES 128 pour générer un code MIC (_Message Integrity Code_) pour chaque message.

* *Application Session key (AppSKey)* pour du chiffrement point à point du message (d'application à application). C'est aussi une clé AES 128.

Il y a donc deux clés statiques stockées sur le noeud et la gateway

== Sécurité en LoRaWAN (ABP)

[TIP]
. ABP ?
=====
Activation by personalization (ABP). le noeud est préconfiguré avec les clés de session "en dur" : DevAddr, MwkSKey et AppSKey. Aucune possibilité de changer les clés après déploiement. ABP est le plus souvent utilisé pour des grands déploiements.
=====

TODO....

== Implémentation de la cryptographie

=== Hello world, phyPayload !

[TIP]
.Objectif
Ici nous allons déchiffrer "à la main" la payload d'un paquet LoRaWAN. Il s'agit plus précisément de la phyPayload, celle que l'on peut intercepter (avec un autre passerelle par exemple). Ce n'est absolument pas un hack car nous avons accès à toutes les clés générées... De plus le flux MQTT est accesible en clair. Le but est de comprendre le chiffrement mis en oeuvre et la structure des paquets LoRaWAN.

==== Récupérer toutes les clés de chiffrement

Comme nous venons de le voir :

* le noeud embarque la _AppKey_ qui est une clé secrète connue aussi du serveur LoRaWAN. C'est le secret partagé.
* Lors de la procédure de _join_, le noeud et le serveur définissent deux clés de session dérivées de la _AppKey_ :
** Network Session Key (NwkSkey)
** Application Session key (AppSKey)

Ces deux dernières clés sont stockées en mémoire vive sur le noeud et ne changent pas durant toute la durée de connexion (TODO : À confirmer).

On peut donc récupérer toutes ces clés avec une modification simple du code embarqué sur le noeud.

On ajoute le code suivant dans notre skecth :

[source,c]
----
// \brief return the current session keys returned from join.
void LMIC_getSessionKeys (u4_t *netid, devaddr_t *devaddr, xref2u1_t nwkKey, xref2u1_t artKey) {
    *netid = LMIC.netid;
    *devaddr = LMIC.devaddr;
    memcpy(artKey, LMIC.artKey, sizeof(LMIC.artKey));
    memcpy(nwkKey, LMIC.nwkKey, sizeof(LMIC.nwkKey));
}
----

et on l'appelle l'automate LMIC dans le bloc :

[source,c]
----
 case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              Serial.print("netid: "); Serial.println(netid, DEC);
              Serial.print("devaddr: "); Serial.println(devaddr, HEX);
              Serial.print("artKey: ");
              for (int i=0; i<sizeof(artKey); ++i) {
                if (i != 0)
                  Serial.print("-");
                Serial.print(artKey[i], HEX);
              }
              Serial.println("");
              Serial.print("nwkKey: ");
              for (int i=0; i<sizeof(nwkKey); ++i) {
                      if (i != 0)
                              Serial.print("-");
                      Serial.print(nwkKey[i], HEX);
              }
              Serial.println("");
}

            // Disable link check validation (automatically enabled
            // during join, but not supported by TTN at this time).
            LMIC_setLinkCheckMode(0);
            break;
----

Lors de la procédure de _join_, on lira dans le moniteur série :

  netid: 0
  devaddr: 593247
  artKey: F7-29-3D-74-57-DD-ED-8B-21-9-89-98-48-19-34-95
  nwkKey: D-22-33-1E-A1-4-3C-38-D3-45-6B-51-8D-7D-19-E0

[TIP]
On peut aussi récupérer ses clés dans l'interface d'administration de loraserver.

Nous avons donc

* Device address : 00593247
* Application session key (artKey) :  F7293D7457DDED8B2109899848193495
* Network session encryption key (nwmMey) : 0D22331EA1043C38D3456B518D7D19E0
* Dans le code, la _AppKey_ est fixée à : 00000000000000000000000000000000

En s'abonnant au flux MQTT de la passerelle `gateway/+/rx`, on voit passer :

  gateway/3150000000000002/rx {"rxInfo":{"mac":"3150000000000002","timestamp":810156987,"frequency":868300000,"channel":1,"rfChain":1,"crcStatus":1,"codeRate":"4/5","rssi":-59,"loRaSNR":8.8,"size":21,"dataRate":{"modulation":"LORA","spreadFactor":7,"bandwidth":125},"board":0,"antenna":0},"phyPayload":"QEcyWQDCAQADBwEydIFTnJG/1DGF"}


Comme nous l'avons vu, la payload a été chiffrée deux fois en AES128 : une fois avec la clé d'application (AppSKey) et une fois avec la clé de réseau (NwkSKey). Voici quelques payloads successives. On voit que la plupart du texte chiffré varie à chaque fois :

QEcyWQDCAQADBwEydIFTnJG/1DGF
QEcyWQDCAgADBwGj/lQyPonTT2JD
QEcyWQDCAwADBwFRghM4kgf0TbxU
QEcyWQDCBAADBwGbxnlIxJ9vSP8K
QEcyWQCABQABAFLbnP0e+aFMzA==
QEcyWQCABgABx4VOzRcncgrReQ==
QEcyWQCABwAB1GOiHsCSPF0FbA==
QEcyWQCACAABOc/qa6Pz8meWCA==
QEcyWQCACQAB52tKleR9eWCukg==
QEcyWQCACgABZ88CTgX2m0WhPg==
QEcyWQCACwABfx7Dk4HJjHE31A==
QEcyWQCADAABUdGFyvfIj/v+Vg==
QEcyWQCADQABL10QcX7gZ5xM7w==
QEcyWQCADgABNJ/OQm1fW66plA==
QEcyWQCADwABgH8JNXv61Qyuew==
QEcyWQCAEAABMXgpiSY17QaYdA==
QEcyWQDAEQABeFpyNh9lZUqNqg==
QEcyWQCAEgABflsMzfdG1nM0xw==

==== Où sont les données à retrouver ?

Elle se trouvent dans la `phyPayload`. Selon les spécifications LoRaWAN 1.1 page 16, c'est une suite d'octets avec la structure suivante :

.PHYPayload
[cols="2,2,2,2"]
|===
|*Taille (octets)*
|1
|7... M
|4

|*PHYPayload*
|MHDR
|MACPayload
|MIC
|===

* MHDR est le _MAC Header_. Spécifie le type de message (_join request_, _data up_, _data down_...) et la version du format de la trame LoRaWAN générée.

* MACPayload : ces octets contiennent le _frame header_ (FHDR), suivi de façon optionnelle du _Fport_ et de notre message _frame payload_ (FRMPayload)
** FHDR
+
.Frame Header (FHDR)
[cols="2,2,2,2,2"]
|===
|*Taille (octets)*
|4
|1
|2
|0.. 15

|*FHDR*
|DevAddr (adresse du noeud)
|FCtrl (octet de controle)
|FCnt (2 octets pour le compteur de trames)
|FOpts (entre 0 et 15 octets pour les options)
|===
+
** Fport pour éxécuter des commandes côté application. Doit être
** FRMPayload : notre message chiffré avant la calcul du code d'intégrité (MIC)
+
En résumé voici la structure des données qui nous intéresse
+
.MACPayload
[cols="2,2,2,2"]
|===
|*Taille (octets)*
|7 à 22
|0 à 1
|0 à N

|*MACPayload*
|FHDR
|FPort
|FRMPayload
|===
+
+
* MIC : le _message integrity code_ est calculé sur les champs : MHDR|FHDR|FPort|FRMPayload. Selon les spécifications LoRaWAN 1.1, ce code MIC est calculé par `cmacS = aes128_cmac(SNwkSIntKey, B1 | msg)` où `B1` est un bloc de donnée (contenant l'adresse du noeud, la longueur de message, etc...) et `msg`, les champs définis ci-dessus. `SNwkSIntKey` est la clé dérivée à partir des AppKey et Nwkkey.


=== Suite...

Ligne 681 : The encryption scheme used is based on the generic algorithm described in IEEE

802.15.4/2006 Annex B [IEEE802154] using AES with a key length of 128 bits.

En fait, l'algorithme de chiffrement AES128 est utilisé pour générer un flux de clés (_keystream_). Pour chaque clé, le chiffré de la payload est obtenu en faisant un XOR entre le clair de la payload et la clé.

Ce n'est bien entendu pas un chiffrement optimal en théorie car une personne très motivée pourrait déchiffrer la payload... Il lui faudra un très grand nombre de messages chiffrés pour arriver à découvrir la payload en clair.

Voir https://www.elektormagazine.com/news/lorawan (à vérifier dans les specifications)

Please see https://docs.loraserver.io/lora-app-server/integrate/data/ 140 for accessing the decrypted application payloads.

When you want to decode & decrypt a LoRaWAN PHYPayload yourself, please see: https://godoc.org/github.com/brocaar/lorawan#example-PHYPayload--Decode 142

For just decrypting the FRMPayload, please see: https://godoc.org/github.com/brocaar/lorawan#EncryptFRMPayload 114

=== Failles de sécurités ?

* You need much less skills to read any key: simply connect 3 wires (tx rx gnd) to the LoRa module and read the keys easily, using the standard commands, published in the documentation.


https://github.com/anthonykirby/lora-packet

* Les clés de chiffrement sont des constantes dans le code.

* Les clés sont stockées en mémoire flash ou EEPROM (non sécurisées)

* Dès qu'une des deux clés est compromise... probable lorsque le noeud est accessible physiquement ou non surveillé, il doit être possible de dupliquer la clé

* Une fois la compromission faite, il est impossible de changer la clé AES sur le noeud (il n'y a pas d'autre canal securisé pour accéder au noeud)

== Bonnes pratiques

* Ne pas utiliser ABP

* Prévoir une autre bande sécurisée pour changer la AppKey.

* utiliser une longueur de messages fixe.

* Générer des clés uniques pour chaque noeud à l'aide d'un bon générateur de nombres peudos-aléatoires (RNG)

* Use OTAA provisioning where the keys/certificates are dynamically negotiated between a device and the Network and Application Servers for each session. Then, force network rejoin in the device periodically to change session keys. This is in contrast to ABP provisioning method which permanently sets both session keys, leading to vulnerability you pointed out.

* If using ABP method, make session keys unique to each device. If one gets compromised, others are unaffected

* Use a secure hardware element in a device to store the security credentials (en Bluetooth ou autre) mais aussi pour effectuer les opérations relatives à la vérofication d'intégrité des messages, le chiffrement et le déchiffrement. This will make it very hard to reverse-engineer the keys by scanning device memories. Additionally, use secure boot to ensure integrity of device firmware.

* Ajouter une couche supplémentaire de chiffrement à la couche application. On peut utiliser un chiffrement asymétrique.

* Always enable uplink/downlink message counter checks in the Network Server to prevent replay attacks

* You can use your own private network server and/or application server with own gateways to prevent unauthorized access at the cloud level

* Envoyer des messages aléatoires à intervalles irréguliers pour éviter les attaques de collecte de métadonnées et masquer l'activité (activités du noeud en fonction des événements (quelqu'un sonne à la porte un message est envoyé...)

LoRaWAN was designed for hardware-constrained devices, so it had to balance many tradeoffs. Also, keep in mind that most applications are for sending data from distributed sensors to the cloud, not for controlling ATM cash dispensers. For typical applications, it's an easy to use, cheap, and effectively secure solution.


== Ressources

Approche de la cryptanalyse de la séquence des blocs de chiffrement : https://www.miscmag.com/lorawan-deploiement-dune-infrastructure-de-test-partie-2-2/

Webinar de The things network "LoRaWAn Security" : https://www.youtube.com/watch?v=Nu_yZelDMZI&feature=player_embedded par @johamstokking johan@thehingsnetwork.org

https://security.stackexchange.com/questions/126987/security-of-an-iot-network-using-aes-lorawan
