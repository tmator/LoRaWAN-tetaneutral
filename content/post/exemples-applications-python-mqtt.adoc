---
title: Un client MQTT en python
tags: ["MQTT", "python", "Paho"]
date: "2018-12-13"
---

:toc: left
:toclevels: 3
:data-uri:
:imagesdir: ./images
:icons: font
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

= Client MQTT en +python+

== Introduction

Loraserver utilise https://fr.wikipedia.org/wiki/MQTT[MQTT] ce qui nous permet de communiquer avec nos objets ou gateway de manière simple.

Voici quelques topics *MQTT* disponible :

*Uplink*: application/[_applicationID_]/device/[_devEUI_]/rx

*Downlink*: application/[_applicationID_]/device/[_devEUI_]/tx

*Status*: application/[_applicationID_]/device/[_devEUI_]/status

*Ack*: application/[_applicationID_]/device/[_devEUI_]/ack

*Error*: application/[_applicationID_]/device/[_devEUI_]/error

L'exemple ci-dessous nous montre comment récupérer les messages envoyés par un device particulier.

Il faut au préalable installer la librairie python paho :

....
pip install paho
....

== Source complet

[source,python]
----
include::src/Python-MQTT-SimpleSub/mqtt-simple.py[]
----
